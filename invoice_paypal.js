
$(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
  if (data.flagName == 'invoice_paid') {
    if (data.flagStatus == 'flagged') {
      $('div.invoice_paypal_button_container').hide();      
    }
    if (data.flagStatus == 'unflagged') {
      $('div.invoice_paypal_button_container').show();
    }
  }
});